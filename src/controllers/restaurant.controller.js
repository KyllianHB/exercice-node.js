const Restaurant = require('../data/models/Restaurant')

// CREATION RESTAURANT

const createRestaurant = async (restaurant, callback) => {
  const _restaurant = new Restaurant({
    name: restaurant.name,
    description: restaurant.description,
    adress: restaurant.adress,
    imagepath: restaurant.imagepath,
    plat: restaurant.plat
  })

  const savedRestaurant = await _restaurant.save()
  const savedRestaurantObject = savedRestaurant.toObject()

  return callback(null, savedRestaurantObject)
}

module.exports = {
  createRestaurant
}
