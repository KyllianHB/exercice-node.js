// --- Appel des variables d'environnement ---
require('dotenv').config()

// --- Initialisation du serveur ---
const express = require('express')

const app = express()
const port = 3000

app.listen(port, () => {
  console.log('Serveur OK | port ' + port)
})

// --- Connextion a la base de données ---
const connect = require('./data/helpers/db')
connect()

// --- Paramètrage d'Express ---
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

// --- Logger ---
const morgan = require('morgan')

app.use(morgan('combined'))

// --- Routes ---
// User
app.use('/user', require('./routes/user'))
// Login
app.use('/auth', require('./routes/auth'))
// Restaurant
app.use('/restaurant', require('./routes/restaurant'))
// Plat
app.use('/plat', require('./routes/plat'))
