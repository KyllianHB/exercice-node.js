const mongoose = require('mongoose')

const { Schema } = mongoose

const restaurantSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true,
    default: ''
  },
  imagepath: {
    type: String,
    defaut: ''
  },
  price: {
    type: Number,
    required: true
  },
  restaurant: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  }]
}, { timestamps: true })

module.exports = mongoose.models.Restaurant || mongoose.model('Restaurant', restaurantSchema)
