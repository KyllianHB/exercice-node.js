# Exercice-Node.js

Exercice dans le cadre du cours de Node.js qui permet de revoir tout ce qu'on à fait.

## Liste des étapes

1. Créer un dossier
2. NPM / ou Yarn Init le projet
3. Installation des paquets
4. Initialiser le projet GIT / Github 
> Envoyer le lien sur Teams 
> Ajouter le .gitignore
5. Base de donnée 
> MongoDB Atlas
6. Base de donnée dans le code avec les variables d'environnement
7. Création des dossiers
8. Modèles de données (Restaurant / Plat / User)
9. Contrôleurs
10. Routes
11. Authentification

## Consignes exercice

**Une liste de restaurants :**
- nom
- description
- adresse
- photo (upload)
- plats : Plats
 
**Les restaurants contiennent des plats :**
- titre
- description
- photo (upload)
- prix 
- restaurant : Restaurant
 
**User :**
- prénom
- nom
- email
- mot de passe
- rôle
---
- si user connecté et = admin il peut créer, modifier et supprimer les plats / restaurants
- consultation des plats et restaurants en public 
- quand je supprime un restaurant, je dois supprimer ses plats

