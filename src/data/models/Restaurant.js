const mongoose = require('mongoose')

const { Schema } = mongoose

const restaurantSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true,
    default: ''
  },
  adress: {
    type: String,
    required: true
  },
  imagepath: {
    type: String,
    defaut: ''
  },
  plat: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'plat'
  }]
}, { timestamps: true })

module.exports = mongoose.models.Restaurant || mongoose.model('Restaurant', restaurantSchema)
