const mongoose = require('mongoose')

const connect = () => {
  mongoose.set('strictQuery', false)
  mongoose.connect(`mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}@${process.env.MONGODB_HOSTNAME}/${process.env.MONGODB_DATABASE}?retryWrites=true&w=majority`)
    .then(() => {
      console.log('BDD OK')
    })
    .catch((error) => {
      console.error('ERREUR de connexion BDD : ' + JSON.stringify(error))
    })
}

module.exports = connect
