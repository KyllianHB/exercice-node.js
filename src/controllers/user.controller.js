
const User = require('../data/models/User')

// CCREATION UTILISATEUR

const createUser = async (user) => {
  if (!user.email || !user.password) {
    throw new Error('Missing data')// On renvoi un code d'erreur s'il manque le mail ou le mot de passe
  }

  const _user = new User({
    firstname: user.firstname,
    lastname: user.lastname,
    phone: user.phone,
    email: user.email,
    password: user.password,
    role: user.role
  })

  // On enregistre l'utilisateur et on recupere la donnée creee dans MongoDB
  const savedUser = await _user.save()
  // On transforme le résultat en objet
  const savedUserObject = savedUser.toObject()
  // On retire le password de l'objet renvoyé
  delete savedUserObject.password
  // On renvoit l'utilisateur dans la réponse de l'API
  return savedUserObject
}

module.exports = {
  createUser
}
