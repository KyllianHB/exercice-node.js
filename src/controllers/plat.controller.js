const Plat = require('../data/models/Plat')

// CREATION RESTAURANT

const createPlat = async (plat, callback) => {
  const _plat = new Plat({
    title: plat.title,
    description: plat.description,
    imagepath: plat.imagepath,
    price: plat.adress,
    restaurant: plat.restaurant
  })

  const savedPlat = await _plat.save()
  const savedPlatObject = savedPlat.toObject()

  return callback(null, savedPlatObject)
}

module.exports = {
  createPlat
}
