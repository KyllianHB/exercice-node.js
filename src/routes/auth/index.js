const router = require('express').Router()
const { loginUser } = require('../../controllers/auth.controller.js')

router.route('/login')
  .post(async (req, res) => {
    const credentials = req.body

    try {
      loginUser(credentials, (error, result) => {
        console.log(error)
        return res.send(result)
      })
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  })

module.exports = router
