const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')

const { Schema } = mongoose

const userSchema = new Schema({
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  phone: {
    type: String
  },
  email: {
    type: String,
    unique: true,
    required: true,
    match: /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    enum: ['admin', 'customer'],
    default: 'customer',
    required: true
  }
}, { timestamps: true })

// Remplace le mdp du user par un equivalent crypté avant l'enregistrement dans la BDD
userSchema.pre('save', function (next) {
  // On récupère le user au travers du contexte "this"
  const user = this
  // On regarde si le mot de passe a changé OU si le user est nouveau
  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, (error, salt) => {
      if (error) {
        throw new Error(error)
      }
      // On hash le mdp avec
      bcrypt.hash(this.password, salt, (error, hash) => {
        if (error) {
          throw new Error(error)
        }
        user.password = hash

        return next()
      })
    })
  }
})

// Définition d'une méthode pour le schéma d'un utilisateur qui permet de comparer un mot de passe donné à celui qui est enregistré pour l'utilisateur.
userSchema.methods.comparePassword = function (password, callback) {
  // Utilisation de la bibliothèque bcrypt pour comparer le mot de passe donné à celui qui est enregistré pour l'utilisateur. La fonction de comparaison prend en entrée le mot de passe à tester, le mot de passe enregistré et une fonction de rappel qui sera appelée une fois la comparaison terminée.
  bcrypt.compare(password, this.password, (error, isMatch) => {
    // Si une erreur se produit lors de la comparaison, on la passe à la fonction de rappel via son premier argument.
    if (error) return callback(error, null)
    // Si aucune erreur ne se produit, on appelle la fonction de rappel avec un deuxième argument qui indique si le mot de passe donné correspond à celui qui est enregistré pour l'utilisateur (true) ou s'ils sont différents (false).
    else return callback(null, isMatch)
  })
}

module.exports = mongoose.models.User || mongoose.model('User', userSchema)
