const router = require('express').Router()
const { createRestaurant } = require('../../controllers/restaurant.controller')

router.route('/create')
  .post(async (req, res) => {
    const infoRestaurant = req.body

    try {
      createRestaurant(infoRestaurant, (error, result) => {
        console.log(error)
        return res.send(result)
      })
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  })

module.exports = router
