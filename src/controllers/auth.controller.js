const User = require('../data/models/User')
const jwt = require('jsonwebtoken')

const loginUser = async (credentials, callback) => {
  let _error

  if (!credentials.email || !credentials.password) {
    _error = 'Invalid Credentials'
  }

  const user = await User.findOne({ email: credentials.email })

  if (!user) {
    _error = 'Invalid Credentials'
    return callback(_error, null)
  }

  user.comparePassword(credentials.password, (error, isMatch) => {
    if (error) _error = 'Invalid Credentials'
    // Resultat de la comparaison dans un booleen isMatch
    if (isMatch) {
      const payload = {
        id: user.id,
        role: user.role
      }
      jwt.sign(payload, process.env.TOKEN_SECRET, { expiresIn: '7d' }, (error, token) => {
        if (error) {
          _error = 'Invalid Credentials'
        }
        // On supprime le mdp du user récupéré en base et on retourne le token
        const _user = user.toObject()
        delete _user.password

        return callback(error, {
          _user,
          token
        })
      })
    } else {
      _error = 'Invalid Credentials'
      return callback(_error, null)
    }
    if (error) {
      _error = 'Invalid Credentials'
      return callback(_error, null)
    }
  })
}

module.exports = {
  loginUser
}
