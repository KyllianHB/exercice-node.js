const { createUser } = require('../../controllers/user.controller')
const router = require('express').Router()

router.route('/create')
  .post(async (req, res) => {
    try {
      // appel de la méthode du controller
      // console.log(req)

      const userCreate = createUser(req.body)
      return res.send(userCreate)
    } catch (error) {
      // En cas d'erreur on renvoie une erreur 500 + le détail dans la réponse
      return res.status(500).send(error)
    }
  })

module.exports = router
