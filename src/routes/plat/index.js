const router = require('express').Router()
const { createPlat } = require('../../controllers/plat.controller')

router.route('/create')
  .post(async (req, res) => {
    const infoPlat = req.body

    console.log(req)
    try {
      createPlat(infoPlat, (error, result) => {
        console.log(error)
        return res.send(result)
      })
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  })

module.exports = router
